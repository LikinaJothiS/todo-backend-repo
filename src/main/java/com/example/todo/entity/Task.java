package com.example.todo.entity;

import com.example.todo.dto.TaskDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="todo")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String task;
    String status;

    public Task() {
    }

    public Task(Integer id, String task, String status) {
        this.id = id;
        this.task = task;
        this.status = status;
    }

    public static Task entityFrom(TaskDto taskDto) {

        return new Task(null, taskDto.getTask(), taskDto.getStatus());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task1 = (Task) o;
        return Objects.equals(id, task1.id) && Objects.equals(task, task1.task) && Objects.equals(status, task1.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, task, status);
    }
}
